package main

import "testing"

func TestPlus(t *testing.T) {
	type args struct {
		x int64
		y int64
	}
	tests := []struct {
		name string
		args args
		want int64
	}{
		{
			name: "4 * 4",
			args: args{
				x: 4,
				y: 4,
			},
			want: 16,
		},
		{
			name: "5 * 5",
			args: args{
				x: 5,
				y: 5,
			},
			want: 25,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Plus(tt.args.x, tt.args.y); got != tt.want {
				t.Errorf("Plus() = %v, want %v", got, tt.want)
			}
		})
	}
}
