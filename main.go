package main

import (
	"fmt"
	"math/rand"
	"time"
)

var (
	Zero         string = "nol"
	UnderTen     string = "dibawah 10"
	UnderHundred string = "dibawah 100 diatas 10"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func main() {
	for {
		x := RandomInt(0, 10)
		y := RandomInt(0, 10)
		res := Plus(x, y)
		resString := fmt.Sprintf("%v ditambah %v = %v", x, y, res)
		fmt.Println(resString)
		time.Sleep(1 * time.Second)
	}
}

func Plus(x int64, y int64) int64 {
	resString := fmt.Sprintf("[INFO] processing function plus %v + %v ", x, y)
	fmt.Println(resString)

	if x == 0 || y == 0 {
		return 0
	}

	return x * y
}

func RandomInt(min, max int64) int64 {
	return min + rand.Int63n(max-min+1)
}
